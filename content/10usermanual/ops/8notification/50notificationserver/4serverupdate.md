+++
title = "更新通知服务器"
description = "更新通知的订阅信息，例如：变更邮箱地址、Webhook 的 URL 地址。  "
weight = 4
+++

更新通知服务器的信息，例如：服务器配置信息等。不支持更新名称。

**操作步骤**

1. 登录平台，切换至管理视图。

2. 在左侧的导航栏中单击 **通知** > **通知服务器**，进入通知服务器列表界面。

2. 单击待查看 ***通知服务器名称***，进入通知服务器详情页面。

3. 单击详情页面右上角的 **操作** 下拉按钮，在展开的下拉列表中选择 **更新**。

4. 进入 **更新通知服务器** 页面后，参照 [创建通知服务器]({{< relref "10usermanual/ops/8notification/50notificationserver/2servercreate.md" >}}) 更新除名称之外的配置信息。
