+++
title = "通知对象（Alpha）"
description = "支持管理通知对象。"
weight = 30
alwaysopen = false
+++

通知对象是接受通知的实体。

可提前设置好通知对象，也可在创建通知时添加通知对象。


{{%children style="card" description="true" %}}





