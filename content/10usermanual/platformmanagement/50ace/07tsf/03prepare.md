+++
title = "部署前准备"
description = ""
weight = 3
alwaysopen = false
+++

1. 在容器平台创建或接入业务集群，用于部署 TSF 组件。记住集群名称，后续部署将通过集群名称指定选中该集群。  
**注意**：用于部署 TSF 组件的业务集群不可以部署其它产品或应用，否则可能导致 TSF 组件运行异常。  

	参考下表，规划 TSF 集群主机的配置和角色： 
	
	| **主机名** | **K8s 角色** | **TSF 角色** | **CPU、内存、磁盘** | **备注** |
	| :--- |:--- | :----- | :---- | :------ |
	| tsf-init | node | 运营组件节点 | 4 核、8Gi、200 GB | 独占 80 端口；在去污点可调度的前提下，也可以使用 K8s Master 节点。 |
	| tsf-node1 | node | 业务组件节点 | 8 核、32Gi、200 GB |  |
	| tsf-node2 | node | 业务组件节点 | 8 核、32Gi、200 GB |  |
	| tsf-node3 | node | 业务组件节点 | 8 核、32Gi、200 GB |  |
	
2. 下表为各组件与运行节点的对应关系。依据下表，开放各个节点主机的端口（TSF 目前使用 Host 模式启动）。

	| **组件信息** | **tsf-init** | **tsf-node1** | **tsf-node2** | **tsf-node3** |
	| :--- |:--- | :----- | :---- | :------ |
	| tsf-init | 80<br>3306<br>6379<br>7001<br>8081<br>9000<br>17600 |  |  |  |
	| oss-consul-client |  | 8301<br>8500<br>8600 | 8301<br>8500<br>8600 | 8301<br>8500<br>8600 |
	| oss-consul-server |  | 8040<br>8041<br>8042<br>8043<br>8044 | 8040<br>8041<br>8042<br>8043<br>8044 | 8040<br>8041<br>8042<br>8043<br>8044 |
	| tsf-mysql |  | 31306 | 31306 |  |
	| tsf-ctsdb |  | 9201<br>19201 | 9201<br>19201 | 9201<br>19201 |
	| tsf-redis |  | 6379 | 6379 |  |
	| tsf-elasticsearch |  | 5601<br>9200<br>19200 | 5601<br>9200<br>19200 | 5601<br>9200<br>19200 |
	| tsf-resource |  | 16000 | 16000 |  |
	| tsf-consul-register |  | 8050<br>8051<br>8052<br>8053<br>8054 | 8050<br>8051<br>8052<br>8053<br>8054 | 8050<br>8051<br>8052<br>8053<br>8054 |
	| tsf-consul-config |  | 8060<br>8061<br>8062<br>8063<br>8064 | 8060<br>8061<br>8062<br>8063<br>8064 | 8060<br>8061<br>8062<br>8063<br>8064 |
	| tsf-consul-authen |  | 8070<br>8071<br>8072<br>8073<br>8074 | 8070<br>8071<br>8072<br>8073<br>8074 | 8070<br>8071<br>8072<br>8073<br>8074 |
	| tsf-consul-access |  | 8000 | 8000 | 8000 |
	| tsf-mesh-apiserver |  | 9010 | 9010 | 9010 |
	| tsf-mesh-pilot |  | 9100<br>9875<br>15001<br>15010<br>15012 | 9100<br>9875<br>15001<br>15010<br>15012 |  |
	| tsf-mesh-mixs |  | 9300<br>9310<br>9876 | 9300<br>9310<br>9876 |  |
	| tsf-repository-access |  | 8100 | 8100 |  |
	| tsf-repository-server |  | 11873<br>15001 | 11873<br>15001 |  |
	| tsf-masterapi |  | 12300 | 12300 |  |
	| tsf-master |  | 8200 | 8200 |  |
	| tsf-controler |  | 8201 | 8201 |  |
	| tsf-trans-coordinator |  | 9710 | 9710 | 9710 |
	| tsf-alarm |  | 26000 | 26000 |  |
	| tsf-apm |  | 11000 | 11000 |  |
	| tsf-auth |  | 12000 | 12000 |  |
	| tsf-dcfg |  | 13000 | 13000 |  |
	| tsf-dispatch |  | 15000 | 15000 |  |
	| tsf-monitor |  | 17500 | 17500 |  |
	| tsf-ms |  | 14000 | 14000 |  |
	| tsf-ratelimit |  | 23000 | 23000 |  |
	| tsf-route |  | 20000 | 20000 |  |
	| tsf-scalable |  | 17000 | 17000 |  |
	| tsf-template |  | 24000 | 24000 |  |
	| tsf-tx |  | 18000 | 18000 |  |
	| tsf-metrics |  | 11112 | 11112 |  |
	| tsf-record |  | 25000 | 25000 |  |
	| tsf-ratelimit-master |  | 6666 | 6666 |  |
	| tsf-gateway |  | 22100 | 22100 |  |
	| license-server |  | 7100 | 7100 |  |

3. 检查四台节点机器上的 hosts 配置，如果 localhost 为"::1"，需要修改成"127.0.0.1"。

	```
	# 修改前
	cat /etc/hosts
	::1 localhost.localdomain localhost
	...
	# 修改后
	cat /etc/hosts
	127.0.0.1 localhost.localdomain localhost
	...
	```

4. 登录 TSF 运营组件节点（tsf-init），创建 tsf 命名空间。

	```
	kubectl create ns tsf
	```
		