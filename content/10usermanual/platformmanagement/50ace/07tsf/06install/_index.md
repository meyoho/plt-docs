+++
title = "部署说明"
description = ""
weight = 6
alwaysopen = false
+++

本节说明如何使用产品运营中心，部署 TSF。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击 **产品运营中心**。

3. 在 **全部产品** 列表，找到 **TSF for Alauda** 产品，单击对应的 ![](/img/ace/003point.png) > **部署**。

4. 根据页面提示，依次完成 [选择目标集群与部署运营组件]({{< relref "10usermanual/platformmanagement/50ace/07tsf/06install/01cluster.md" >}})、[配置参数]({{< relref "10usermanual/platformmanagement/50ace/07tsf/06install/02config.md" >}})、[部署业务组件]({{< relref "10usermanual/platformmanagement/50ace/07tsf/06install/03job.md" >}})，部署完成后进行 [检查与调试]({{< relref "10usermanual/platformmanagement/50ace/07tsf/06install/04check.md" >}})。