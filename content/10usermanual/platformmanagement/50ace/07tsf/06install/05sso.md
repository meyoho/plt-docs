+++
title = "SSO 对接"
description = ""
weight = 5
alwaysopen = false
+++

<style>ol ol { list-style-type: lower-roman;}</style>

用户在平台登录认证后，通过平台的 TSF 产品入口访问 TSF，将无需再进行登录认证。用户在 TSF 产品进行登出操作，同时也会登出平台。

参考以下说明完成 SSO 对接。

**cpaas-fit 组件部署**

cpaas-fit 部署完成后，可以获取的 url 列表如下。

<table>
  <tr>
    <th>名称</th>  
    <th>说明</th>
    <th>部署方式</th>
    <th>URL</th>
  </tr>
  <tr>
    <td rowspan="2">登录</td>
    <td rowspan="2">第三方产品登录地址</td>
    <td>ClusterIP</td>
    <td>http://{平台的访问地址}/connector/app/{name}</td>
  </tr>
  <tr>
    <td>NodePort</td>
    <td>http://{global 集群任一控制节点的 IP}:{service 的 nodeport}/app/{name}</td>
  </tr>
  <tr>
    <td rowspan="2">登出</td>
    <td rowspan="2">第三方产品登出地址</td>
    <td>ClusterIP</td>
    <td>http://{平台的访问地址}/connector/app/{name}</td>
  </tr>
  <tr>
    <td>NodePort</td>
    <td>http://{global 集群任一控制节点的 IP}:{service 的 nodeport}/app/{name}</td>
  </tr>
  <tr>
    <td rowspan="2">认证</td>
    <td rowspan="2">第三方产品 Token 校验地址</td>
    <td>ClusterIP</td>
    <td>http://{平台的访问地址}/connector/app/{name}/serviceValidate</td>
  </tr>
  <tr>
    <td>NodePort</td>
    <td>http://{global 集群任一控制节点的 IP}:{service 的 nodeport}/app/{name}/serviceValidate</td>
  </tr>
</table>
  
在平台 global 集群的任一控制节点执行以下操作。

1. 获取组件物料包，并上传到平台 global 集群的任一控制节点。当前最新版本为 cpaas-fit-v2.8.2.tgz，MD5 为 63a8654ba4629b65994ef1cd21ddf6b7。

	![tsf](/img/ace/tsf/sso1.png?classes=big)
	
2. 解压物料包，查看物料包中的目录结构。

	```
	[root@master1 ~]# tar xvf cpaas-fit-v2.8.2.tgz
	[root@master1 ~]# cd cpaas-fit/
	[root@master1 cpaas-fit]# tree
	.
	├── alauda-fit-v2.8.2.tar      ## 这是fit的镜像，需要将镜像移出chart目录
	├── Chart.yaml
	├── README.md
	├── templates
	│   ├── configmap.yaml
	│   ├── deployment.yaml
	│   ├── _helpers.tpl
	│   ├── ingress.yaml
	│   └── service.yaml
	└── values.yaml
	  
	1 directory, 9 files
	```
3. 上传镜像到平台的镜像仓库。

	```
	# 将镜像移出 Chart 目录
	[root@master1 cpaas-fit]# mv alauda-fit-v2.8.2.tar ..
	[root@master1 cpaas-fit]# ls
	Chart.yaml  README.md  templates  values.yaml
	# 上传镜像
	[root@init ~]# docker load -i alauda-fit-v2.8.2.tar
	[root@init ~]# docker tag 10.0.32.123:60080/alaudak8s/alauda-fit:v2.8.2 {registry.address:port}/alaudak8s/alauda-fit:v2.8.2
	[root@init ~]# docker push {registry.address:port}/alaudak8s/alauda-fit:v2.8.2
	```

4. 安装 cpaas-fit 组件。

	```
	# 部署命令
	helm install cpaas-fit/ -n <RELEASE_NAME> --namespace=<NAME_SPACE> --set global.registry.address=<REGISTRY_ADDR> --set global.host=<ACE_DOMAIN_NAME>
	# ACE3.0 域名访问部署命令示例
	helm install cpaas-fit/ -n cpaas-fit --namespace=cpaas-system --set global.registry.address=10.0.32.144:60080 --set global.host=tsfdemo.alauda.cn
	# ACE3.0 IP 访问部署命令示例
	helm install cpaas-fit/ -n cpaas-fit --namespace=cpaas-system --set global.registry.address=10.0.32.144:60080
	```
	
	Chart 安装的参数说明如下。
	
	| **参数名** | **类型** | **说明** | **推荐配置** |
	| :--- |:--- | :----- | :---- |
	| RELEASE_NAME | 必填 | 自定义 chart 的 release name | cpaas-fit |
	| NAME_SPACE | 必填 | 自定义 chart 安装所属的命名空间 | cpaas-system |
	| REGISTRY_ADDR | 必填 | 平台镜像仓库的地址 |  |
	| ACE_DOMAIN_NAME | 非必填 | 当平台是通过域名访问时，此参数必填。<br>当平台是通过 IP 访问时，此参数不设置。 |  |
	
5. 当平台访问协议为 https 时，需要修改 service ；当平台访问协议为 http 时，此步骤跳过。

	```
	kubectl edit svc cpaas-fit  -n cpaas-system
	#将 spec.type 从 ClusterIP 修改为 NodePort
	#删除 spec.ports 的 port，k8s 会自动分配一个 NodePort
	```
	
	修改完成后的 YAML 示例如下。
	
	```
	apiVersion: v1
	kind: Service
	metadata:
	  creationTimestamp: "2020-03-24T14:18:44Z"
	  labels:
	    chart: cpaas-fit-v2.8.7-2.0
	    heritage: Tiller
	    release: cpaas-fit
	    service_name: cpaas-fit
	  name: cpaas-fit
	  namespace: cpaas-system
	  resourceVersion: "19873705"
	  selfLink: /api/v1/namespaces/cpaas-system/services/cpaas-fit
	  uid: 5e48f2b9-6dda-11ea-ae0d-525400543d9a
	spec:
	  clusterIP: 10.105.37.63
	  externalTrafficPolicy: Cluster
	  ports:
	  - name: metrics
	    nodePort: 30452
	    port: 8080
	    protocol: TCP
	    targetPort: 8080
	  selector:
	    service_name: cpaas-fit
	  sessionAffinity: None
	  type: NodePort
	status:
	  loadBalancer: {}
	```

6. 修改 fit-configmap 配置，修改命令示例如下。

	```
	kubectl edit cm fit-configmap -n cpaas-system
	```
	
	参数配置说明如下。
	
	* `authentication.oidc_issuer_url` 配置 dex 服务地址，必须为 https 协议，例如 `https://{平台访问域名或IP}/dex`。

	* `authentication.oidc_client_id` 配置容器平台 的client_id，默认为 `alauda-auth`。

	* `connectors` 串接第三方产品服务列表。

		* `name` 服务名称，英文小写，例如 `tsf`。此处参数决定 cpaas-fit 的 url path，例如 `http://129.28.192.160:30452/app/{name}` 后面的 `{name}` 参数。

		* `type` connector类型，必须为 `tsf`。

		* `url` 第三方产品服务的登入地址，例如 `http://94.191.124.31`。

	修改完成后的 YAML 示例。

	```
	apiVersion: v1
	data:
	  config.yaml: |
	    authentication:
	      oidc_issuer_url: https://tsfdemo.alauda.cn/dex
	      oidc_client_id: alauda-auth
	    connectors:
	    - name: tsf
	      type: tsf
	      url: http://94.191.124.31
	    - name: csp
	      type: csp
	      url: http://example.com
	kind: ConfigMap
	metadata:
	  creationTimestamp: "2020-03-24T14:18:44Z"
	  name: fit-configmap
	  namespace: cpaas-system
	  resourceVersion: "19475245"
	  selfLink: /api/v1/namespaces/cpaas-system/configmaps/fit-configmap
	  uid: 5e47d2ee-6dda-11ea-ae0d-525400543d9a
	```	
	
	完成这步操作后，根据 cpaas-fit 的 service 类型，可以获得 TSF 在 cpaas-fit 的访问路径。
	
	* cpaas-fit 的 service 为 ClusterIP  
	http://{平台的访问地址}/connector/app/{name}
	
	* cpaas-fit 的 service 为 NodePort  
	http://{global 集群任一控制节点的 IP}:{service 的 nodeport}/app/{name}
	
7. 修改 dex-configmap  
	在 redirectURIs 增加 TSF 在 cpaas-fit 的访问路径，见上一步骤。修改命令示例如下。
	
	```
	kubectl edit cm dex-configmap -n cpaas-system
	```
	
	修改完成后的 YAML 示例如下。
	
	```
	apiVersion: v1
	data:
	  config.yaml: "expiry:\n signingKeys: 876000h\n idTokens: 876000h\nissuer: https://tsfdemo.alauda.cn/dex\nstorage:\n
	    \ type: kubernetes\n  config:\n    inCluster: true\nweb:\n  https: 0.0.0.0:5556\n
	    \ tlsCert: /etc/dex/tls/tls.crt\n  tlsKey: /etc/dex/tls/tls.key\ntelemetry:\n
	    \ http: 0.0.0.0:8080\noauth2:\n  responseTypes:\n  - code\n  - token\n  - id_token\n
	    \ \n  skipApprovalScreen: true\nstaticClients:\n- id: alauda-auth\n  redirectURIs:\n
	    \ - https://tsfdemo.alauda.cn/\n  - https://tsfdemo.alauda.cn/console\n  - https://tsfdemo.alauda.cn/devops\n
	    \ - https://tsfdemo.alauda.cn/console-acp/\n  - https://tsfdemo.alauda.cn/console-devops/\n
	    \ - https://tsfdemo.alauda.cn/console-asm/\n  - https://tsfdemo.alauda.cn/console-aml/\n
	    \ - https://tsfdemo.alauda.cn/console-aml\n  - https://tsfdemo.alauda.cn/console-platform/\n
	    \ - https://tsfdemo.alauda.cn/console-acp\n  - https://tsfdemo.alauda.cn/console-devops\n
	    \ - https://tsfdemo.alauda.cn/console-asm\n  - https://tsfdemo.alauda.cn/console-platform\n
	    \ - https://tsfdemo.alauda.cn/console-ace/\n  - http://k8s.alauda.io/\n  - http://k8s.alauda.io\n
	    \ - http://129.28.192.160:30452/app/tsf\n  name: 'Alauda auth'\n  secret: ZXhhbXBsZS1hcHAtc2VjcmV0\n\nauthConfig:\n
	    \ name: auth-config\n  client_id: alauda-auth\n  enabled: true\n  custom_redirect_uri:
	    \"{\\\"console-acp\\\":\\\"https://tsfdemo.alauda.cn/console-acp\\\",\\\"console-devops\\\":\\\"https://tsfdemo.alauda.cn/console-devops\\\",\\\"console-asm\\\":\\\"https://tsfdemo.alauda.cn/console-asm\\\",\\\"console-platform\\\":\\\"https://tsfdemo.alauda.cn/console-platform\\\",\\\"amp\\\":\\\"http://k8s.alauda.io\\\"}\"\n
	    \ response_type: code\n  scopes: \"openid,profile,email,groups,ext\"\n  auth_namespace:
	    cpaas-system\n\nenablePasswordDB: true\nstaticPasswords:\n- email: admin@cpaas.io\n
	    \ hash: $2a$10$HLxKjJQs9YxEOZXCUPih4.dIhwUT03cg58zt8AqcNnw9gyN01tXBi\n  username:
	    admin\n  userID: 08a8684b-db88-4b73-90a9-3cd1661f5466\n"
	kind: ConfigMap
	metadata:
	...	
	```

8. 验证部署成功

	```
	#fit-configmap 中,tsf 的 type 为 tsf
	curl http://129.28.192.160:30452/app/tsf/serviceValidate -H "X-Auth-Token:{TOKEN}"
	#fit-configmap 中,tsf 的 type 为 common
	curl http(s)://{acp访问地址}/connector/app/coding/serviceValidate?ticket={TOKEN}
	```
	
	返回信息中 message 为 OK，并且 data 包含用户信息，则表示部署成功。
	
	![tsf](/img/ace/tsf/sso2.png?classes=big)
	
**TSF 配置**

TSF 配置完成后，可以通过 cpass-fit 的登录 URL 进行 TSF 产品登录操作。在 TSF 集群的 init 节点执行以下操作。

1. 配置 auth 模块，配置文件路径 /opt/access/auth/application.yml。

	配置项说明
	
	| **参数地址** | **配置值** | **说明** |
	| :--- |:--- | :----- |
	| sso.logout-url | cpaas-fit 的登出 URL | 注销 token 的地址 |
	| sso.check-token-url | cpaas-fit 的认证 URL | 校验 token 的地址 |	
	| sso.server | 必须为：proxy | 对接第三方 sso 登录的类型 |
	
	具体操作命令
	
	```
	# 进入auth目录
	cd /opt/accesss/auth
	# 停止auth进程
	./stop.sh
	# 备份配置文件
	cp application.yml application.yml.bak-20200326
	# 编辑auth配置文件的 sso 属性块
	vim application.yml	
	```	
	
	配置完成后的示例如下
	
	```
	...
	sso:
	  server: proxy
	  check-token-url: http://129.28.192.160:30452/app/tsf/serviceValidate
	  logout-url: http://129.28.192.160:30452/app/tsf
	```	
	
2. 配置 gateway 模块，配置文件路径 /opt/access/gateway/application.yml

	```
	# 进入gateway目录
	cd /opt/accesss/gateway
	# 停止gateway进程
	./stop.sh
	# 备份配置文件
	cp application.yml application.yml.bak-20200326
	# 编辑gateway配置文件 的 access.auth 属性块
	vim application.yml
	# 设置 permission-allowed 字段值为 false
	# 启动 gateway
	./start.sh
	```

3. 配置 tsf-frontend 模块，配置文件路径 /opt/tsf-frontend/nginx.conf.sso	
	
	```
	# 进入tsf-frontend目录
	cd /opt/tsf-frontend
	# 备份nginx.conf.sso配置文件
	cp nginx.conf.sso nginx.conf.sso.bak-20200326
	# 编辑 nginx.conf.sso 的配置
	vim nginx.conf.sso
	# 执行更新脚本
	sh ./update.sh
	# 解压部署包
	tar  -xzf tsf-frontend-backup.tar.gz
	# 将部署包移到上层目录保存
	mv tsf-frontend-backup.tar.gz ../
	# 移动部署包内容到当前目录下
	mv ./opt/tsf-frontend/* ./
	# 清楚空文件
	rm -rf ./opt
	# 重启服务
	./restart.sh
	```
	
	其中 nginx.conf.sso 配置说明
	
	```
	1. location /login (25行)
	
	rewrite /login(.*) {cpaas-fit 的登录 URL}?service={TSF 的产品地址} redirect;
	# 例子如下：
	rewrite /login(.*) http://129.28.192.160:30452/app/tsf?service=http://94.191.124.31/ redirect;
	2. /logout (35行)
	
	rewrite /logout {cpaas-fit 的登出 URL}?service={TSF 的产品地址} redirect;
	# 例子如下：
	rewrite /logout http://129.28.192.160:30452/app/tsf?service=http://94.191.124.31/ redirect;
	```