+++
title = "部署前准备"
description = ""
weight = 3
alwaysopen = false
+++

<style>ol ol { list-style-type: lower-roman;}</style> 

## 部署规划

**部署架构**

![redis](/img/ace/redis/redis01.png?classes=big)

Redis平台整体架构如下：

* **控制台**：该模块为前台管理界面，实际的图形化操作界面，可以对资源、实例进行日常的管理和维护操作。比如实例的生产、监控告警配置、资源的上架等。

* **管控 Control_Center（CC）**：该模块为后台服务，主要负责实例的生命周期管理、资源管理、HA 等。

* **Cache_agent（ccagent）**：该模块为后台服务的 Agent 节点，部署再实际的 IaaS 资源上。

* **监控系统**：该模块负责监控 Redis、Proxy 及机器的存活状态，并上报相应的监控数据到对应的存储系统，目前使用的存储系统是 InfluxDB。

**环境要求**


| **设备类型** | **设备要求** | **最小配置** | **设备数量** |
| :--- |:--- | :-------------------- |:--- |
| Proxy 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 | 2 台 |
| Cache 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 | 主从版 4 台，集群版 7 台 |
| 管控设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，64GB 磁盘 | 2 台 |
| LVS 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 | 和管控设备混部 0 台，独立部署 2 台，准备 LVS 的负载地址（LVS_VIP） |
| InfluxDB 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 | 和管控设备混部 0 台，独立部署 1 台 |
| MySQL 设备 | 物理机/虚拟机 | 4 核 CPU，8GB 内存，256GB 磁盘 | 1 台，版本要求 5.6+，使用 MySQL 高可用版或 TDSQL 集中式版 |		
	
**部署模式**

包括最小混合部署、最小部署、高可用部署三种模式。

* **最小混合部署**：1 台管控机器，且 LVS、InfluxDB 均部署于管控机器之上；Proxy 混合部署于 Cache 机器之上。

	* 实现方式：手动连接MySQL数据库，选择管控数据库，从 interface_machine_t 表中查询出机器信息，然后插入到 redis_machine_t 表中。
	
	* 机器数量
	
		* 主从版：管控机器（包含 LVS、InfluxDB）1 台，Proxy 机器 2 台，Cache 机器 4 台。
	
		* 集群版：管控机器（包含 LVS、InfluxDB）1 台，Proxy 机器 2 台，Cache 机器 6 台。
	
		注：混合部署的机器容量必须低于物理容量的一半。
	
* **最小部署**：1 台管控机器，且 LVS、InfluxDB 均部署于管控机器之上。

	* 机器数量
	
		* 主从版：管控机器（包含 LVS、InfluxDB）1 台，Proxy 机器 2 台，Cache 机器 4 台。
	
		* 集群版：管控机器（包含 LVS、InfluxDB）1 台，Proxy 机器 2 台，Cache 机器 7 台。
	
	![redis](/img/ace/redis/redis02.png?classes=big)


* **高可用部署**：两台管控机器，且 LVS、InfluxDB 分别单独部署于各自机器之上。

	![redis](/img/ace/redis/redis03.png?classes=big)

	* 机器数量 	
	
		* 情况1：管控机器（包含 LVS、InfluxDB）2 台
		
		* 情况2：管控机器 2 台，LVS 2 台，InfluxDB 1 台
		
		* 情况3：管控机器（包含 LVS）2 台，InfluxDB 1 台
	
	以上三种情况，如果是主从版 Redis，需加 2 台 Proxy，4 台 Cache；如果是集群版 Redis，需要 2 台 Proxy，7 台 Cache。
	
	注：如果 InfluxDB 不在 CC 本机，独立部署的话，此时部署 CC 组件后需将管控数据库中 sys_misc_config_t 表中 Influxdb_url 更改为实际部署 Influxdb 服务的机器 IP。同时需要修改 cc 组件中的配置文件 MulServer.conf。
	
## 系统参数配置

**部署环境设置**

1. 扩缩容说明

	问题描述：扩缩容时 control_center 机器会频繁的连 cache_agent 机器，由于每次连接都在很短的时间内结束，会导致很多的 TIME_WAIT，以至于会用光可用的端口号，所以新的连接没办法绑定端口，会报错 "Cannot assign requested address"(/data/log/cluster_cc/cc-debug.log)。
	使用命令 `netstat -a|grep TIME_WAIT`，即可发现很多 TIME_OUT 状态的连接。
	
	解决办法：用以下命令修改内核参数（需要 root 权限） --所有机器
	追加以下参数到 /etc/sysctl.conf
	
	```
	net.ipv4.tcp_tw_reuse=1
	net.ipv4.tcp_tw_recycle = 1
	net.ipv4.tcp_timestamps = 1
	net.ipv4.tcp_fin_timeout = 30
	```
	
	最后生效以上设置：
	
	```
	sysctl -p
	```

2. 系统参数优化 --所有 cache_agent 机器

	追加以下参数到 /etc/sysctl.conf
	
	```
	net.core.somaxconn = 511
	vm.swappiness = 1
	```
	
	最后生效以上设置：
	
	```
	sysctl -p
	```

3. 执行以下命令关闭 Transparent Huge Pages(THP)，注意不同 linux 版本路径稍有不同 --所有 Cache/Proxy 机器

	```
	echo never > /sys/kernel/mm/transparent_hugepage/enabled
	```
 
	注意：如果需要重启生效，需要追加命令到 /etc/rc.local

4. 查看系统文件句柄数 ulimit -n，如果小于 65535，修改最大文件句柄数 --所有Cache/Proxy机器
	
	```
	ulimit -Sn 65535
	```
	
	注意：如果需要重启生效，需要追加如下配置到 /etc/security/limits.conf
	
	```
	* soft nofile 65535
	
	* hard nofile 65535
	```

**MySQL 客户端配置**

MYSQL_CLIENT_PATH=mysql

**注意**：默认配置 mysql，如果没有将 mysql 命令加入系统搜索路径，请配置绝对路径，例如：/usr/local/mysql/bin/mysql，否则数据库初始化会失败。

## 数据目录准备

1. 分别登录 control_center、cache_agent、LVS、InfluxDB 机器

2. 每台机器数据都保存在 data 目录，如果实际安装机器有单独的数据盘做存储，需要挂载数据盘到 /data 目录
	
	1. `fdisk -l`，会显示类似 /dev/vdb 这样的名称。
	
	2. `mkfs.xfs disk_name`，disk_name 名称就是上一步的结果。
	
	3. 加到开机启动,追加到 /etc/fstab，如：
	
		/dev/vdb   /data    xfs  defaults   1 1
	
	4. 使得挂载生效 `mount -a`


