+++
title = "CSP"
description = ""
weight = 6
alwaysopen = false
+++

CSP（Cloud Storage for Private）是一种私有云存储平台工具，面向企业客户提供可扩展、高可靠、强安全、低成本的PB级海量数据存储能力，同时保证核心敏感数据私密性。

