+++
title = "部署说明"
description = ""
weight = 6
alwaysopen = false
+++

本节说明在完成部署前准备后，如何使用产品运营中心部署 TDSQL。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击 **产品运营中心**。

3. 在 **全部产品** 列表，找到 **TDSQL** 产品，单击对应的 ![](/img/ace/003point.png) > **部署**。

4. 根据页面提示，依次完成 [核心组件部署]({{< relref "10usermanual/platformmanagement/50ace/03tdsql/06install/01core.md" >}})、[赤兔初始化]({{< relref "10usermanual/platformmanagement/50ace/03tdsql/06install/02chitu.md" >}})、[调度组件部署]({{< relref "10usermanual/platformmanagement/50ace/03tdsql/06install/03scheduler.md" >}})、[可靠性组件部署]({{< relref "10usermanual/platformmanagement/50ace/03tdsql/06install/04availability.md" >}})。
