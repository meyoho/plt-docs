+++
title = "可靠性组件部署"
description = ""
weight = 4
alwaysopen = false
+++

根据部署规划，在 **部署信息** 区域，勾选要部署的可靠性组件，并配置参数。安装以下组件之前要确保 TDSQL 核心组件安装无误后再进行。

**说明**：
 
* 如果需要使用 HDFS 异地冷备存储 binlog 和镜像文件，用于数据回档恢复，勾选安装 HDFS。

* 如果需要使用 TDSQL 默认提供的接入层 LVS，勾选安装 LVS。

* 如果需要使用多源同步功能，勾选安装 Kafka 和 Consumer。

* 如果需要使用网关日志分析及 SQL 审计功能，勾选安装 ES。

<style>ol ol { list-style-type: lower-roman;}</style> 

**HDFS**

1. 勾选确认已完成 **数据目录准备** 和 **主机名设置** 的准备工作，并在机器上完成了相关配置。详情参考 [部署前准备]({{< relref "10usermanual/platformmanagement/50ace/03tdsql/03prepare.md" >}}) 文档。

2. 继续设置以下参数。

	* **SSH 端口**：实际的 SSH 通信端口。 

	* **数据文件路径**：HDFS 的存放数据文件的路径（一个磁盘一个路径，逗号分隔）。例如：`/data2/hdfs/data,/data3/hdfs/data,/data4/hdfs/data`。这些数据文件路径应有 tdsql 系统账号权限。

	* **主机 IP**：设置要部署 HDFS 组件主机的 IP，数量必须为 1 （单点）或 3（高可用），且大于 Zookeeper 的 IP 个数。支持选择或添加 IP。

3. 部署完成后续操作：

	* **注意**：用 tdsql 系统账号执行 `jps` 命令，可以看到 HDFS 的进程名称（只适用于高可用 3 节点架构）。  
	hdfs1 和 hdfs2 机器上应该看到 NameNode、JournalNode、DataNode、DFSZKFailoverController；hdfs3 机器上应该看到DataNode、JournalNode。
	
	* 登录赤兔，到 "集群管理->集群配置” 中设置 “HDFS服务列表"，设置 HDFS 监控。

	 ![tdsqlstructure](/img/ace/tdsql/hdfs.png?classes=big)

	如果是 3 节点的 HDFS 架构，如上图所示填写 namenode 节点（一般 2 个）的 50070 端口。生产环境不能用单节点 HDFS，单节点架构的 HDFS 的端口号是 9870。

**Kafka**

1. 设置以下参数。

	* **日志文件路径**：Kafka 的日志文件路径（一个磁盘一个路径，逗号分隔），需要较大的磁盘空间。
	
	* **主机 IP**：设置要部署 Kafka 组件主机的 IP，数量必须为 3。支持选择或添加 IP。

**Consumer**

**注意**：**Consumer** 组件依赖于 **HDFS** 和 **Kafka** 组件。必须选择部署 **HDFS** 和 **Kafka** 组件，方可正常部署 **Consumer** 组件，否则将会导致部署失败。

1. 设置以下参数。

	* **主机 IP**：设置要部署 Kafka 组件主机的 IP。支持选择或添加 IP。

	* **网卡名称**：Consumer 通信网卡的设备名。

**LVS**

LVS 机器建议单独部署，不建议和其它组件复用（最多只能和 Zookeeper 复用）。

1. 设置以下参数。

	* **主机 IP**：设置要部署 LVS 组件主机的 IP，数量必须为 2。支持选择或添加 IP。

2. 部署完成后续操作：

	1. 登录赤兔，上报两台 LVS 机器的信息。

		* 设备 IP：填入 LVS 机器的通信 IP 地址。
		
		* 网卡名字：通信网卡设备名。

		* 子网掩码：填入实际的子网掩码。

		* IDC：LVS 机器需要划分到同一个 IDC 中，比如这里的“IDC_SZ_YDGL_0401_000001”。

		![tdsqlstructure](/img/ace/tdsql/lvs1.png?classes=big)
		
	2. 创建 VIP（不能选取部署的机器 IP 作为 VIP，需要另选之外一个）。

		* VIP：规划一个 VIP 的地址，VIP 的地址要和 LVS 机器的通信 IP 地址在同一个网段内。
		
		* 设备 IP 列表：填入 LVS 机器的通信 IP 地址，一行一个。
		
		![tdsqlstructure](/img/ace/tdsql/lvs2.png?classes=big)
		
	3. 创建 VIPGroup。

		* VIPGroup 名：新建一个 VIPgroup，取一个有意义的名字。

		![tdsqlstructure](/img/ace/tdsql/lvs3.png?classes=big)
		
	4. 给 VIPGroup 添加 VIP。

		* LVSGroup 名：VIPgroup 名称。
		
		* 添加 VIP：填入 VIP 地址。

		![tdsqlstructure](/img/ace/tdsql/lvs4.png?classes=big)


**ES**

部署 ES 前，请确保已选择部署 Kafka，否则会导致 ES 部署失败。

1. 设置以下参数。

	* **内存**：占用内存，单位为 G。例如输入 8，将占用 8G*2=16G 的内存。
	
	* **日志存放天数**
	
	* **主机 IP**：设置要部署 ES 组件主机的 IP。支持选择或添加 IP。

2. 部署 ES 完毕后，请到 ES 主机做以下操作，启动相关服务。

	1. 使用 tdsql 系统用户，执行以下命令，启动 ES 的 Master 控制节点服务。

		`
		su - tdsql
		` 
		
		`
		cd /data/application/es-install/es/master/tools
		`
		  
		`
		./start-elasticsearch.sh`
		
	2. 使用 tdsql 系统用户，执行以下命令，启动 ES 的 Data 数据节点服务。

		`
		su - tdsql
		` 
		
		`
		cd /data/application/es-install/es/data/tools
		`
		  
		`
		./start-elasticsearch.sh`
		
	3. 使用 root 用户，执行以下命令，启动日志收集服务 Logstash。

		`
		cd /data/application/es-install/logstash/tools
		`  
		
		`
		./start-logstash.sh`
	
	4. 使用 root 用户，执行以下命令，启动日志相关辅助服务 es-cleaner。

		`
		cd /data/application/es-install/es-cleaner
		`
		  
		`
		./start-es-cleaner.sh`
		
	5. 使用 root 用户，执行以下命令，启动 ES 的前端搜索界面服务 Kibana。

		`
		cd /data/application/es-install/kibana-5.6.4-linux-x86_64/bin/
		`
		  
		`
		nohup ./kibana &`		

3. ES 的验证及相关配置。

	1. 输入 ES IP 地址，打开 ES 前台页面。

	2. 启动 ES 后，需要把实例详情中的 “网关审计数据是否入库” 打开，选择“是”。只有在打开这个开关后的日志才会入 ES，为了可以定位问题，需要的话一定要打开。

		![tdsqlstructure](/img/ace/tdsql/es.png?classes=big)

**赤兔备机**

1. 设置以下参数。
	
	* **备机 IP**：设置要部署 赤兔 组件备机的 IP，数量必须为 1。支持选择或添加 IP。

**执行部署**

1. 设置完成所有可选组件参数后，在 **可靠性组件部署** 区域，单击 **查看**，概览部署组件信息并确认信息正确。

2. 单击 **开始部署**，等待部署结果。

	* 如提示部署失败，可查看日志、文档，修改未完成部署组件的参数。单击 **继续部署**，继续部署未完成的组件。
	
	* 如提示部署成功，单击 **完成**。  
	
	**注意**：
	
	* 如部署了 HDFS，参考上文进行后续的 HDFS 监控配置。
	
	* 如部署了 LVS，参考上文进行后续的 LVS 配置。
	
	* 如部署了 ES，参考上文进行后续的主机操作，以及前台验证与配置。
