+++
title = "统一门户（Portal）"
description = ""
weight = 1
alwaysopen = false
+++

部署平台时，如启用 PaaS 云平台及相关产品，登录平台时，将直接进入统一门户（Portal）。

**Portal 图片替换**

![banner](/img/ace/portal/banner1.png?classes=big)

如上图，上下 banner 分别为：banner-top.png 和 banner-bottom.png。参考以下说明，管理员可以根据需要替换 Portal 中的图片。

1. 获取储存图片信息的 ConfigMap，此处以 configmap ui-logo中的 favicon.ico 为例。

	![banner](/img/ace/portal/banner2.png?classes=big)


2. 使用 mountPath 进行挂载可替换图片。

	在镜像中的位置为：
	
	/underlord/dist/static/banners/banner-top.png
	
	/underlord/dist/static/banners/banner-bottom.png
	
	![banner](/img/ace/portal/banner3.png?classes=big)


**产品资源概览**

展示已使用的云产品以及产品业务信息总览，相关产品及信息如表所述。

| **产品名称** | **产品类型** | **业务数据统计项** |
| :--- |:--- | :-------------------- |
| 容器平台 | 计算 | 命名空间个数、应用个数、Pod 个数 |
| DevOps 平台 | 应用 | 流水线条数、质量阀通过率 |
| 微服务治理平台 | 应用 | 微服务个数、工作负载个数 |
| TI Matrix | 应用 | 模型服务个数、AI 任务个数 |
| TSF | 应用 | 应用个数、运行服务个数 |
| TDSQL | 中间件 | 分布式实例个数、非分布式实例个数 |
| CSP | 存储 | 总存储空间、已使用存储空间 |


**所有云产品及服务**

显示所有已部署的云产品（包含运行中、运行异常、升级中、删除失败状态）以及平台公共服务。

单击 ***产品名称+版本*** 链接，可在新标签页中打开相应的产品界面。

