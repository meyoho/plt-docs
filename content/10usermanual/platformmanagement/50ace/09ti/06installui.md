+++
title = "部署说明（可视化模式）"
description = ""
weight = 6
alwaysopen = false
+++

本节说明如何使用产品运营中心，以可视化模式部署 TI Matrix。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击 **产品运营中心**。

3. 在 **全部产品** 列表，找到 **TI Matrix** 产品，单击对应的 ![](/img/ace/003point.png) > **部署**。

4. 单击 **可视化部署**。在 **主机配置** 环节，配置以下参数。

	**默认 SSH 配置**
	
	* **用户名**：登录主机用户名，如：root。

	* **密码**：登录主机用户密码，如：123456。

	* **端口**：主机的 SSH 端口，如：22。

	**主机配置**：
	
	单击 **添加主机**，可添加多个要安装 TI Matrix 的主机，为每个主机设置以下参数。
	
	* **IP**：主机的 IP 地址。

	* **SSH 配置**：目前支持 **默认**，使用默认 SSH 配置。
	
	![tiproject](/img/ace/ti/install1.png?classes=big)  
	
	完成后，单击 **下一步**。
	
5. 在 **基础组件配置** 环节，为各个 TI Matrix 基础组件选择部署的主机（可选择的主机为第一步添加的主机）。  
需要配置的组件包括 

	* **Time Server**：必填，单选。
	
	* **Time Client**：必填，多选，和 Time Server 不能在同一个主机上。
	
	* **Consul Server**：必填，多选。  
	**注意**：部署 Consul Server 的主机应同时部署 Mysql Server 和 Redis Server。
	
	* **HPAPI Server**：必填，单选。 

	![tiproject](/img/ace/ti/install2.png?classes=big)

	完成后，单击 **下一步**。

6. 在 **高级组件配置** 环节，为各个 TI Matrix 高级组件选择部署的主机（可选择的主机为第一步添加的主机），或接入使用外部已有的组件。

	**Mysql Server**
	
	* 如选择 **部署组件**，选择要部署该组件的主机，常见情况下为两个。  
	**注意**：部署 Mysql Server 的主机应同时部署 Consul Server 和 Redis Server。
	
	* 如选择 **接入组件**，需配置以下参数。

		* **地址**：输入 Msyql Server 地址，如：192.168.1.3。

		* **端口**：输入 Mysql 端口，如：3306。

		* **用户名**：输入 Mysql 用户名，如：timatrix。

		* **密码**：输入 Mysql 用户密码，如：123456。

	**Redis**
	
	* 如选择 **部署组件**，选择要部署 Master 的主机和 Slave 的主机。  
	**注意**：部署 Redis Server 的主机应同时部署 Consul Server 和 Mysql Server。
	
	* 如选择 **接入组件**，需配置以下参数。
	
		* **服务地址**：输入 Redis 服务的域名或 IP 地址。
	
		* **密码**：输入 redis 服务的用户密码，如：123456。
	
		* **端口**：输入 redis 服务的端口号，如：6379。
	
		* **DNS 地址**：输入 Redis 的 DNS 域名地址。

	**Zk Server**
	
	* 如选择 **系统默认组件**，将默认使用系统提供的组件，选择默认组件所在的主机。

	* 如选择 **接入组件**，配置以下参数。

		* **服务地址**：输入 Zk server 的服务地址。

	**Kfk Server**
	
	* 如选择 **系统默认组件**，将默认使用系统提供的组件，选择默认组件的地址。

	* 如选择 **接入组件**，配置以下参数。

		* **服务地址**：输入 Kfk server 服务的域名或 IP 地址。
	
		* **Web 地址**：输入 Kfk server Web 的域名或 IP 地址。

	**ES Master**
	
	* 如选择 **系统默认组件**，将默认使用系统提供的组件，配置以下参数。
		
		* **主机**：选择默认组件所在的主机。
	
		* **端口**：显示 elasticsearch 服务的端口，不支持修改。

		* **用户名**：输入 elasticsearch 服务的的用户名。

		* **密码**：输入 elasticsearch 服务的的用户密码。
	

	* 如选择 **接入组件**，配置以下参数。

		* **主机**：输入 elasticsearch 服务的域名或 IP 地址。
	
		* **端口**：输入 elasticsearch 服务的端口。

		* **用户名**：输入 elasticsearch 服务的的用户名。

		* **密码**：输入 elasticsearch 服务的的用户密码。
	
	![tiproject](/img/ace/ti/install3.png?classes=big)
	
	所有高级组件配置完成后，单击 **下一步**。
	
7. 在 **监控组件配置** 环节，为各个 TI Matrix 监控组件（包括 Grafana、telegraf 和 InfluxDB）选择部署的主机（可选择的主机为第一步添加的主机）。

	* **Master 主机**：选择部署监控组件的 Master 主机。

	* **Slave 主机**：选择部署监控组件的 Slave 主机。

	![tiproject](/img/ace/ti/install4.png?classes=big)

	所有监控组件配置完成后，单击 **下一步**。
	
8. 在 **核心组件配置** 环节，配置以下参数。

	* **集群**：选择要部署核心组件的集群，应选择 **部署前准备** 第一步中创建的 timatrix 项目所属的集群。建议和第一步中主机所在的集群相同。

	* **镜像仓库地址**：输入组件镜像所在仓库的地址，应输入的地址为 **部署前准备** 第二步中，导入了 TI 镜像的镜像仓库地址。

	* **产品版本**：显示当前 TI Matrix 产品版本。

	* **GPU 节点**（可选）：选择 Kubernetes 集群中的 GPU 主机。

	* **File Server**（可选）：选择部署 File Server 的主机。

	* **SIP Server**（可选）：选择部署 SIP Server 的主机。

	* **SIP Streaming**（可选）：选择部署 SIP Streaming 的主机。
	
	* **K8s Master**：选择 Kubernetes 集群中的 Master 节点。

	* **路径**（可选）：输入要使用的本地存储路径。 

	![tiproject](/img/ace/ti/install5.png?classes=big)

9. 所有参数配置完成后，单击 **部署**。状态变为 **部署中**，等待部署完成。

	![tiproject](/img/ace/ti/install6.png?classes=big)
	
	部署完成后，在业务集群 Master 节点执行以下命令，部署算法认证 插件。
	
	`
	helm install . --name hostdev --namespace alauda-system  --set global.registry.address=harbor-b.alauda.cn --set global.namespace=alauda-system
	`
	
	部署完成后，支持进行系统配置，参考 [系统配置]({{< relref "10usermanual/platformmanagement/50ace/09ti/08monitorconfig.md" >}}) 文档。
