+++
title = "OIDC"
description = "支持平台管理员在平台上添加 LDAP，对接企业的 LDAP（Lightweight Directory Access Protocol，轻量级目录访问协议）服务，可将企业已有的用户体系同步至平台。"
weight = 1
alwaysopen = false
+++

平台支持 OIDC（OpenId Connect）协议，由平台管理员添加 OIDC 配置后，可使用第三方账号登录平台。

同时，支持平台管理员更新、删除已添加的 OIDC。

{{%children style="card" description="true" %}}
