+++
title = "LDAP"
description = "支持平台管理员在平台上添加 LDAP，对接企业的 LDAP（Lightweight Directory Access Protocol，轻量级目录访问协议）服务，可将企业已有的用户体系同步至平台。"
weight = 1
alwaysopen = false
+++

支持平台管理员在平台上添加 LDAP，对接企业的 LDAP（Lightweight Directory Access Protocol，轻量级目录访问协议）服务，可将企业已有的用户体系同步至平台。

同时，支持平台管理员更新、删除已对接的 LDAP 服务。

{{%children style="card" description="true" %}}
