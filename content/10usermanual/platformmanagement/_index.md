+++
title = "平台管理"
description = ""
weight = 5
alwaysopen = false
+++

平台管理页面仅对拥有平台管理员角色的用户可见。

<img src="/img/00platformmanagement.png" width = "860" />


{{%children style="card" description="true" %}}




