+++
title = "更新自定义角色的基本信息"
description = "支持更新平台上自定义角色的基本信息，可更新显示名称和描述。"
weight = 5
+++


支持更新平台上自定义角色的基本信息，可更新显示名称和描述。

**前提条件**

您的账号已拥有平台管理员角色。

**操作步骤**

1. 登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击  **角色管理**，展开角色列表。

3. 单击待更新的 ***角色的名称***，进入角色的详情页面。

4. 单击页面右上角的 **操作** 下拉选择按钮，并选择 **更新基本信息**。

5. 在弹出的 **更新基本信息** 对话框中，可更新角色的 **显示名称** 和 **描述**。

6. 单击 **更新** 按钮。


	
	
	

